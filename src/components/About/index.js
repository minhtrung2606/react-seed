/**
 * Dependencies
 */
import React from 'react';

/**
 * Code starts right below
 */

export default (props) => {
  let {user} = props;
  
  return (
    <div className="about-app">
      <header className="root-app-header">
        <h1 className="root-app-title">
          About
        </h1>
      </header>
      <p>
        Hello world!!! <br/>
        My name is {user.userName}
      </p>
    </div>
  );
};