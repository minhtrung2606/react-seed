/**
 * Dependencies
 */
import React from 'react';

/**
 * Modules
 */
import './index.css';
import HNav from '../HNav';

/**
 * Code starts right below
 */

class Header extends React.Component {
  render() {
    return (
      <div className="header display-flex">
        <div className="display-flex primary-color in-dark container">
          <span className="branding title">React Seed</span>
          <HNav></HNav>
        </div>
      </div>
    );
  }
}

export default Header;