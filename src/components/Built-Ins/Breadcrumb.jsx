/**
 * Dependencies
 */
import React from 'react';

/**
 * Components
 */

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

const _renderPaths = paths => {
  return (
    <div className="breadcrumb-paths">{
      (paths || []).map((path, index) => (
        <small key={path} className="breadcrumb-path secondary-color in-white">{path}{index < paths.length - 1 ? ' > ' : ''}</small>
      ))
    }</div>
  );
};

const _renderAppTitle = title => {
  return (<div className="breadcrumb-app-title">
    <h3>{title}</h3>
  </div>);
};

export default ({paths = [], title = 'Unknown App', className = ' '}) => {
  return (
    <div className={'-breadcrumb ' + className}>
      {_renderPaths(paths)}
      {_renderAppTitle(title)}
    </div>
  );
};
