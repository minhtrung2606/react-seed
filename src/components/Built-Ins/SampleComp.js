/**
 * Dependencies
 */
import React from 'react';

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

class SampleComp extends React.Component {
  render() {
    return (
      <div>Sample component works!!!</div>
    );
  }
}

export default SampleComp;
