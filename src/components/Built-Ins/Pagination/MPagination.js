class MPagination {
  static SHIFT_PAGE = '...';

  constructor(totalPages = 1, displayingPage = 1) {
    this.totalPages = totalPages;
    this.displayingPage = displayingPage;
    this.active = 1;
    this.from = 1;
    this.to = Math.min(totalPages, displayingPage);
    this.rawPages = this._genPages();
    this.new = true;
    this.currPages = [];
  }

  _genPages() {
    let totalPages = this.totalPages;
    let pages = [];
    let i;
    
    for (i = 0; i < totalPages; i++) {
      pages.push(i + 1);
    }

    return pages;
  }

  _isActiveHidden(pages = []) {
    return !(pages.indexOf(this.active) + 1); // +1 because of leting valid position at 1 can be detected
  }

  isActive(page) {
    return page === this.active;
  }

  isShiftPage(page) {
    return page === MPagination.SHIFT_PAGE;
  }

  canPrev() {
    return this.active > 1;
  }

  canNext() {
    return this.active < this.totalPages;
  }

  activate(page) {
    this.active = page;
    this._shift(0);
  }

  build() {
    return this._shift(0);
  }

  /**
   * 
   * @param {Number} move direction to be shifted. It could be in one of the followings:
   *  - (-1): shift to the left
   *  - 0: stay the same
   *  - (+1): shift to the right
   */
  _shift(move = 0) {
    let pages;
    let isActiveHidden;
    let lPlaceholders = [];
    let rPlaceholders = [];
    let activePlaceholders = [];

    // 1. Slice
    this.from = Math.min(Math.max(this.from + move, 1), this.totalPages - this.displayingPage + 1);
    this.to = this.from + this.displayingPage - 1;
    pages = this.rawPages.slice(this.from - 1, this.to);
    
    // 2. Re-slice basing on hiding state of the active
    // 3. Calculate active
    isActiveHidden = this._isActiveHidden(pages);
    if (isActiveHidden) {

      // Slice to the right
      if (this.active > pages[pages.length - 1]) {
        // 1.
        pages.pop();

        // 2. Calculate active
        activePlaceholders.push(MPagination.SHIFT_PAGE);
        activePlaceholders.push(this.active);
        pages = pages.concat(activePlaceholders);
      }

      // Slice to the left
      if (this.active < pages[0]) {
        // 1.
        pages.shift();

        // 2. Calculate active
        activePlaceholders.push(this.active);
        activePlaceholders.push(MPagination.SHIFT_PAGE);
        pages = activePlaceholders.concat(pages);
      }
    }

    // 4. Add edge placeholders which are left and right
    if (pages[0] > 1) {
      lPlaceholders.push(MPagination.SHIFT_PAGE);
    }
    if (pages[pages.length - 1] < this.totalPages) {
      rPlaceholders.push(MPagination.SHIFT_PAGE);
    }

    // 5. Gather them all
    if (lPlaceholders.length) {
      pages = lPlaceholders.concat(pages);
    }
    if (rPlaceholders.length) {
      pages = pages.concat(rPlaceholders);
    }

    this.currPages = [].concat(pages);
    return pages;
  }

  next() {
    // 1. Update active
    this.active++;

    // 2. Check if active one is still in pages
    if (this._isActiveHidden(this.currPages)) {
      // Shift to the right
      return this._shift(1);
    } else {
      return this.currPages.concat([]);
    }
  }

  prev() {
    // 1. Update active
    this.active--;

    // 2. Check if active one is still in pages
    if (this._isActiveHidden(this.currPages)) {
      // Shift to the left
      return this._shift(-1);
    } else {
      return this.currPages.concat([]);
    }
  }

  shift(index) {
    let move;
    let pages;
    let currPages = this.currPages;

    if (index === 0) {
      move = -1;
    } else if (index === currPages.length - 1) {
      move = 1;
    } else {
      move = (currPages.indexOf(this.active) < index) ? (-1) : 1;
    }

    pages = this._shift(move);

    this.currPages = [].concat(pages);
    return pages;
  }
}

export default MPagination;