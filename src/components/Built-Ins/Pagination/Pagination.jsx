/**
 * Dependencies
 */
import React from 'react';
import {Pagination as RBPagination, PaginationItem, PaginationLink} from 'reactstrap';
import MPagination from './MPagination';

/**
 * Components
 */

/**
 * Modules
 */

/**
 * Others
 */
import './index.css';

/**
 * Code starts right below
 */

class Pagination extends React.Component {
  constructor(props) {
    super(props);

    this.mPag = new MPagination(10, 5);

    this.state = {
      pages: this.mPag.build()
    };

    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);
  }

  updatePages(pages) {
    this.setState({...this.state, pages})
  }

  next() {
    let pages;
    
    pages = this.mPag.next() || this.state.pages;
    this.updatePages(pages);
  }

  prev() {
    let pages;

    pages = this.mPag.prev() || this.state.pages;
    this.updatePages(pages);
  }

  goToPage(page, index) {
    if (this.mPag.isShiftPage(page)) {
      let pages;
      
      pages = this.mPag.shift(index);
      this.updatePages(pages);
    } else {
      this.mPag.activate(page);
    }
  }

  canNext() {
    return this.mPag.canNext();
  }

  canPrev() {
    return this.mPag.canPrev();
  }

  isActive(page) {
    return this.mPag.isActive(page);
  }

  render() {
    return (<div className="-pagination">
      <RBPagination size="sm" aria-label="Page navigation example">
        <PaginationItem disabled={!this.canPrev()}>
          <PaginationLink previous href="#" onClick={this.prev} />
        </PaginationItem>
        
        {this.state.pages.map((page, index) => (
          <PaginationItem key={index} active={this.isActive(page)} onClick={() => {
            this.goToPage(page, index);
          }}>
            <PaginationLink href="#">{page}</PaginationLink>
          </PaginationItem>
        ))}

        <PaginationItem disabled={!this.canNext()}>
          <PaginationLink next href="#" onClick={this.next} />
        </PaginationItem>
      </RBPagination>
    </div>);
  }
}

export default Pagination;