/**
 * Dependencies
 */
import React from 'react';
import {Button} from 'reactstrap';

/**
 * Components
 */

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

class ButtonBar extends React.Component {
  render() {
    let {buttons = [], onClick = () => {}} = this.props;

    return (<div className="button-bar">{
      buttons.map(btn => (
        <Button
          key={btn.id}
          outline={btn.isOutlineBtn === undefined ? true : btn.isOutlineBtn}
          color={btn.color || 'link'}
          size={btn.size || 'sm'}
          onClick={() => onClick(btn.id)}>
          {btn.label}
        </Button>
      ))
    }</div>)
  }
}

export default ButtonBar;