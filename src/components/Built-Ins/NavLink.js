/**
 * Dependencies
 */
import React from 'react';
import {Link} from 'react-router-dom';

/**
 * Code starts right below
 */

export default ({to, title, className}) => {
  return (
    <Link to={to} className={className}>{title}</Link>
  );
};