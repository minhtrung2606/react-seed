/**
 * Dependencies
 */
import React from 'react';

/**
 * Components
 */
import NavLink from './../NavLink';

/**
 * Modules
 */
import './index.css';
import ROUTES from '../../../routes/const';

/**
 * Code starts right below
 */

class HNav extends React.Component {
  renderNavItems() {
    let navItems = [
      ROUTES.HOME,
      ROUTES.ABOUT,
      ROUTES.SAMPLE_CRUD
    ];

    return navItems.map(({id, to, title}) => (
      <NavLink key={id} to={to} title={title} className="hnav" />
    ));
  }

  render() {
    return (
      <div>
        {this.renderNavItems()}
      </div>
    );
  }
}

export default HNav;