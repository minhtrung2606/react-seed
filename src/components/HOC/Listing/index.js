/**
 * Dependencies
 */
import React from 'react';
import {Input, ListGroup} from 'reactstrap';

/**
 * Components
 */
import Pagination from './../../Built-Ins/Pagination/Pagination';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from '@fortawesome/free-solid-svg-icons';

/**
 * Modules
 */

/**
 * Others
 */
import './index.css';

/**
 * Code starts right below
 */

export default WrappedItem => {

  class Listing extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        searching: false,
        searchStr: ''
      }

      this.toggleSearching = this.toggleSearching.bind(this);
      this.onSearching = this.onSearching.bind(this);
      this.stopSearching = this.stopSearching.bind(this);
    }

    toggleSearching() {
      this.setState({...this.state, searching: !this.state.searching});
    }

    onSearching(e) {
      this.setState({...this.state, searchStr: e.target.value});
    }

    stopSearching(e) {
      e.preventDefault();
      this.setState({...this.state, searchStr: ''});
    }

    render() {
      let {label, items = [], flush = true} = this.props;

      items = items.filter(item => {
        return JSON.stringify(item).indexOf(this.state.searchStr) >= 0;
      });

      return (<div className="listing">
        <h4 style={{display: 'inline'}}>{label}</h4> <FontAwesomeIcon icon={faSearch} onClick={this.toggleSearching} />
        <div className={this.state.searching ? 'search section-margin-bottom' : 'search section-margin-bottom hidden'}></div>
        <Input
          value={this.state.searchStr}
          bsSize="sm"
          placeholder="Search..."
          className={this.state.searching ? 'search' : 'search hidden'}
          onChange={this.onSearching} />
        {items.length === 0 ?
          <small>No item matched search string<a href="#" onClick={this.stopSearching}> Stop searching</a></small> : ''}
        <div className="section-margin-bottom"></div>
        <Pagination></Pagination>
        <ListGroup flush={flush} className="item-list">
          {items.map(item => <WrappedItem item={item} key={item.id} />)}
        </ListGroup>
      </div>);
    }
  }

  return Listing;
};