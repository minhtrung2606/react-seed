/**
 * Dependencies
 */
import React from 'react';
import {ListGroupItem} from 'reactstrap';

/**
 * Components
 */

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

class ListItem extends React.Component {
  render() {
    const {item = {}} = this.props;
    const name = item.make + ' - ' + item.model;
  
    return (<ListGroupItem className="default-list-item">{name}</ListGroupItem>);
  }
}

export default ListItem;