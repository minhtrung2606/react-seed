/**
 * Dependencies
 */
import React from 'react';
import {FormFeedback, FormText} from 'reactstrap';

/**
 * Components
 */

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

export default (WrappedComp) => {

  class ValidatedFC extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        valid: true,
        errMsg: ''
      };

      this.onInputChange = this.onInputChange.bind(this);
    }

    validate(value) {
      let {validator = {}} = this.props;
      let {rules = []} = validator;
      let i, len;
      let valid = true;
      let errMsg;

      for (i = 0, len = rules.length; i < len; i++) {
        const rule = rules[i];

        valid = valid && rule.apply(value);

        if (!valid) {
          errMsg = rule.errMsg;
          break;
        }
      }

      return {valid, errMsg};
    }

    onInputChange(e) {
      let {onChange = () => {}} = this.props;
      let newState = this.validate(e.target.value);

      this.setState(newState);
      onChange(e);
    }

    showErrMsg() {
      let {valid, errMsg} = this.state;
      
      if (!valid) {
        return (<FormFeedback>{errMsg}</FormFeedback>);
      }
    }

    showInfoMsg() {
      let {validator = {}} = this.props;
      let {infoMsg} = validator;

      if (infoMsg) {
        return (<FormText>{infoMsg}</FormText>);
      }
    }

    render() {
      let valid = this.state.valid;
      return (
        <div className="validated-form-control">
          <WrappedComp
            {...this.props}
            bsSize="sm"
            invalid={!valid}
            onChange={this.onInputChange} />
          {this.showErrMsg()}
          {this.showInfoMsg()}
        </div>
      );
    }
  }

  return ValidatedFC;

};