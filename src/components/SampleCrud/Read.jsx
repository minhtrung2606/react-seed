/**
 * Dependencies
 */
import React from 'react';
import { AgGridReact } from 'ag-grid-react';

/**
 * Components
 */
import Pagination from './../Built-Ins/Pagination/Pagination';

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */



class Read extends React.Component {
  render() {
    let {cols, data = []} = this.props;

    if (data.length === 0) {
      data = [
        {id: 'toyota-celica', make: "Toyota", model: "Celica", price: 35000},
        {id: 'ford-mondeo', make: "Ford", model: "Mondeo", price: 32000},
        {id: 'porseche-boxter', make: "Porsche", model: "Boxter", price: 72000}
      ]
    }

    return (<div style={{ height: '50vh'}} className="ag-theme-material">
      <AgGridReact
          columnDefs={cols}
          rowData={data}>
      </AgGridReact>
      <div className="section-margin-bottom"></div>
      <div className="ag-pagination">
        <Pagination></Pagination>
      </div>
    </div>);
  }
}

export default Read;