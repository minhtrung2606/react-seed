/**
 * Dependencies
 */
import React from 'react';
import {AgGridReact} from 'ag-grid-react';

/**
 * Components
 */
import makeListing from './../HOC/Listing/index';
import ListItem from './../HOC/Listing/ListItem';
import Pagination from './../Built-Ins/Pagination/Pagination';

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

const Listing = makeListing(ListItem);

class Manage extends React.Component {
  render() {
    let {cols, items = []} = this.props;

    if (items.length === 0) {
      items = [
        {id: 'toyota-celica', make: "Toyota", model: "Celica", price: 35000},
        {id: 'ford-mondeo', make: "Ford", model: "Mondeo", price: 32000},
        {id: 'porseche-boxter', make: "Porsche", model: "Boxter", price: 72000}
      ]
    }

    cols = [
      {headerName: "Make", field: "make"},
      {headerName: "Model", field: "model"},
      {headerName: "Price", field: "price"}
    ];

    return (<div className="row">
      <div className="col-md-4">
        <Listing label="Products" items={items} />
      </div>
      <div className="col-md-8">
        <div style={{ height: '50vh'}} className="ag-theme-material">
          <h4>Products Accessories</h4>
          <AgGridReact
              columnDefs={cols}
              rowData={items}>
          </AgGridReact>
          <div className="section-margin-bottom"></div>
          <div className="ag-pagination">
            <Pagination></Pagination>
          </div>
        </div>
      </div>
    </div>);
  }
}

export default Manage;