/**
 * Dependencies
 */
import React from 'react';
import {Input, FormGroup, Label, Form} from 'reactstrap';

/**
 * Components
 */
import ValidatedFC from './../HOC/ValidatedFormControl/index';

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

const ValidatedInput = ValidatedFC(Input);

class CreateUpdate extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: ''
    };

    this.validator = {
      rules: [
        {apply: value => { return !!value }, errMsg: 'Value cannot be empty' },
        {apply: value => { return 'TRY ME' !== (value || '').toUpperCase() }, errMsg: 'How dare you try me !!! >"<' }
      ],
      info: ''
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(e) {
    this.setState({...this.state, inputValue: e.target.value});
  }

  render() {
    return (<Form className="_form">
      <FormGroup>
        <Label htmlFor="" size="sm">Validated Input</Label>
        <ValidatedInput
          onChange={this.onInputChange}
          validator={this.validator}
          defaultValue={this.state.inputValue} />
      </FormGroup>
    </Form>);
  }
}

export default CreateUpdate;