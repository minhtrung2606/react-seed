/**
 * Dependencies
 */
import React from 'react';
import Loadable from 'react-loadable';

/**
 * Components
 */
import Breadcrumb from '../Built-Ins/Breadcrumb';
import ButtonBar from '../Built-Ins/ButtonBar';
import Loading from './../Built-Ins/Loading';
import CreateUpdate from './CreateUpdate';

/**
 * Others
 */
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Manage from './Manage';

/**
 * Code starts right below
 */

const Read = Loadable({loader: () => import('./Read'), loading: Loading});

class SampleCrud extends React.Component {
  constructor(props) {
    super(props);

    this.onButtonClick = this.onButtonClick.bind(this);
    this.ACTIONS = {
      CREATE: 1,
      READ: 0,
      UPDATE: 2,
      DELETE: -1,
      MANAGE: 3
    };
    this.BUTTONS = [
      {id: this.ACTIONS.CREATE, label: 'Create', color: 'primary'},
      {id: this.ACTIONS.UPDATE, label: 'Edit'},
      {id: this.ACTIONS.DELETE, label: 'Delete'},
      {id: 'configure', label: 'Configure', isOutlineBtn: false},
      {id: this.ACTIONS.MANAGE, label: 'Manage', isOutlineBtn: false}
    ];
    this.ACTION_BUTTONS = [
      {id: this.ACTIONS.READ, label: '<  Back to landing page', color: 'primary'},
    ];

    this.state = {
      paths: ['Home', 'Management'],
      appTitle: 'Sample CRUD',
      action: this.ACTIONS.READ,
      buttons: this.BUTTONS.concat([])
    };
  }

  componentDidMount() {
    this.props.getAllEntries();
  }

  onButtonClick(id) {
    let buttons = this.BUTTONS.concat([]);

    if (this.ACTIONS.READ !== id) {
      buttons = this.ACTION_BUTTONS.concat([]);
    }

    this.setState({...this.state, action: id, buttons});
  }

  renderContentFromAction(action) {
    let {cols, entries} = this.props;

    switch (action) {
      case this.ACTIONS.CREATE:
      case this.ACTIONS.UPDATE:
        return (<CreateUpdate />);
      case this.ACTIONS.MANAGE:
        return <Manage items={entries}/>
      case this.ACTIONS.READ:
      default:
        return (<Read cols={cols} data={entries}></Read>);
    }
  }

  render() {
    return (
      <div className="sample-crud">
        <div className="app-header">
          {(this.state.action === this.ACTIONS.READ) ?
            <Breadcrumb
              paths={this.state.paths}
              title={this.state.appTitle}
              className="section-margin-bottom" /> : null}
          
          <ButtonBar buttons={this.state.buttons} onClick={this.onButtonClick} />
        </div>
        <div className="section-margin-bottom"></div>
        <div className="app-container">
          {this.renderContentFromAction(this.state.action)}
        </div>
      </div>
    );
  }
}

export default SampleCrud;