import React from 'react';
import {BrowserRouter} from 'react-router-dom';

import './index.css';
import AppRoutes from '../../routes/index';
import Header from '../Built-Ins/Header';

class RootApp extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <div className="root-app">
          <Header></Header>
          <div className="root-app-container container">
            <AppRoutes/>
          </div>
          <footer/>
        </div>
      </BrowserRouter>
    );
  }
}

export default RootApp;
