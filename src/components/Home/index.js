/**
 * Dependencies
 */
import React from 'react';
import {Button} from 'reactstrap'

/**
 * Modules
 */

/**
 * Code starts right below
 */

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.updateUser = this.updateUser.bind(this);
  }

  updateUser() {
    this.props.updateUser({userName: 'React Seed'});
  }

  render() {
    let {user} = this.props;

    return (
      <div className="app">
        <header className="app-header">
          <h1 className="app-title">
            Home
          </h1>
        </header>
        <p>
          You're logged in under the name of: {user.userName}
        </p>
        <div>
          <Button outline onClick={this.updateUser}>
            Update User
          </Button>
        </div>
      </div>
    );
  }
}

export default Home;
