const ROUTES = {
  HOME: {id: 'home', to: '/', title: 'Home'},
  ABOUT: {id: 'about', to: '/about', title: 'About'},
  SAMPLE_CRUD: {id: 'sample-crud', to: '/sample-crud', title: 'Sample CRUD'}
};

export default ROUTES;