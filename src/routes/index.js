/**
 * Dependencies
 */
import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Loadable from 'react-loadable';

/**
 * Components/Containers
 */
import Loading from '../components/Built-Ins/Loading';

/**
 * Modules
 */
import ROUTES from './const';

/**
 * Others
 */

/**
 * Code starts right below
 */

/**
 * All lazy loading components/containers must be explicitly declared here. There's no exception to refactor this thing
 */

const Home = Loadable({loader: () => import('../containers/Home/index'), loading: Loading});
const About = Loadable({loader: () => import('./../containers/About/index'), loading: Loading});
const SampleCrud = Loadable({loader: () => import('./../containers/SampleCrud/index'), loading: Loading})

/**
 * All accessible routes must be declared here
 */
export default () => {
  return (
    <Switch>
      <Route exact path={ROUTES.HOME.to} component={Home}/>
      <Route exact path={ROUTES.ABOUT.to} component={About}/>
      <Route exact path={ROUTES.SAMPLE_CRUD.to} component={SampleCrud} />
    </Switch>
  );
};