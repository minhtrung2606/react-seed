/**
 * Dependencies
 */
import {takeEvery, put} from 'redux-saga/effects'

/**
 * Modules
 */
import Actions from './actions';
import RestAPI from './rest';

/**
 * Others
 */

/**
 * Code starts right below
 */

const SAGA_SAMPLE_ACTION = 'SAGA_ACTIONS_UPDATE';
const SAGE_GET_ALL_ENTRIES = 'SAGE_GET_ALL_ENTRIES';

/**
 * Saga
 * Export saga to be used by the middleware within the store.
 * Once declared this saga, immediately go to ./../../store/index.saga.js to import to the rootSaga
 */

export default [
  _watchDoSample(),
  _watchGetAllEntries()
];

function* _watchDoSample() {
  yield takeEvery(SAGA_SAMPLE_ACTION, _doSample);
}

function* _doSample(action) {
  yield put(Actions.doSample(action.data));
}

function* _watchGetAllEntries() {
  yield takeEvery(SAGE_GET_ALL_ENTRIES, _getAllEntries);
}

function* _getAllEntries() {
  yield put(Actions.gettingAllEntries(true));
  const resp = yield RestAPI.getAllEntries();
  yield put(Actions.getAllEntries(resp.data));
}

/**
 * API
 * Exported APIs used by containers to integrate with Saga
 */

export {
  doSample,
  getAllEntries
};

function doSample(userData) {
  return {
    type: SAGA_SAMPLE_ACTION,
    data: userData
  };
}

function getAllEntries() {
  return {
    type: SAGE_GET_ALL_ENTRIES,
    data: null
  };
}