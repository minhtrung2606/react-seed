/**
 * Dependencies
 */
import CONSTANTS from './constants';

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

/**
 * Reducers
 * Once declared this reducer, immediately go to ./../../store/index.js to import to the store combineReducers section
 * 
 * @param {*} state Current state of this part of the store
 * @param {Object} action A PURE JS object contain the following properties
 * - type: a string representing an action which is defined in the constant.
 * - data: user data which will be used to update the store
 */
export default (state = CONSTANTS.GENESIS_STATE, action) => {
  let newState = state;

  switch(action.type) {
    case CONSTANTS.ACTIONS.SAMPLE_ACTION:
      // It's up to a specific context to have a business logic code to handle user actions when updating the store
      newState = {...state, ...action.data};
      break;
    case CONSTANTS.ACTIONS.GETTING_ALL_ENTRIES:
      newState = {...state, gettingAllEntries: action.data};
      break;
    case CONSTANTS.ACTIONS.GET_ALL_ENTRIES:
      newState = {...state, entries: action.data};
      break;
    default:
      // This must always be here
      // newState = state;
      break;
  }

  return newState;
};
