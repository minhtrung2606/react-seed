/**
 * Dependencies
 */
import * as axios from 'axios';

/**
 * Others
 */

/**
 * Code starts right below
 */

const API_URL = 'http://localhost:3500/api/v1/sample-crud';

export default {
  getAllEntries: () => {
    return axios.get(API_URL);
  }
};
