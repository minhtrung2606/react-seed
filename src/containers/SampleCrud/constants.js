/**
 * Code starts right below
 */

const CONSTANT = {
  /**
   * This is the very initial data of a specific state
   */
  GENESIS_STATE: {
    gettingAllEntries: false,
    entries: []
  },

  /**
   * This defines actions used by either an action creator or a reducer
   */
  ACTIONS: {
    SAMPLE_ACTION: 'SAMPLE_ACTION',
    GET_ALL_ENTRIES: 'GET_ALL_ENTRIES',
    GETTING_ALL_ENTRIES: 'GETTING_ALL_ENTRIES'
  }
};

export default CONSTANT;