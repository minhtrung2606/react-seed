/**
 * Dependencies
 */
import UserConstants from "./constants";

/**
 * Code starts right below
 */

export default {
  update: user => {
    return {
      type: UserConstants.ACTIONS.UPDATE,
      data: user
    };
  }
};