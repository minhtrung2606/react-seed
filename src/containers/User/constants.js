/**
 * Code starts right below
 */

const UserConstants = {
  GENESIS_STATE: {userName: 'Unknown'},
  ACTIONS: {
    UPDATE: 'UPDATE'
  }
};

export default UserConstants;