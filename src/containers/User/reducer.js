/**
 * Dependencies
 */
import UserConstants from './constants';

/**
 * Code starts right below
 */

export default (state = UserConstants.GENESIS_STATE, action) => {
  switch (action.type) {
    case UserConstants.ACTIONS.UPDATE:
      return {...state, ...action.data}
    default:
      return state;
  };
};