/**
 * Dependencies
 */
import {delay} from 'redux-saga';
import {put, takeEvery} from 'redux-saga/effects';

/**
 * Modules
 */
import UserAction from './actions';

/**
 * Code starts right below
 */

const SAGA_ACTIONS_UPDATE = 'SAGA_UPDATE';

/**
 * Saga
 * Export saga to be used by the middleware within the store
 */

export default [
  _watchUpdateUser()
];

function* _watchUpdateUser() {
  yield takeEvery(SAGA_ACTIONS_UPDATE, _updateUser);
}

function* _updateUser(action) {
  yield delay(5000);
  yield put(UserAction.update(action.data));
}

/**
 * API
 * Exported APIs used by containers/components to integrate with Saga
 */

export {
  updateUser
}

function updateUser(user) {
  return {
    type: SAGA_ACTIONS_UPDATE,
    data: user
  };
};
