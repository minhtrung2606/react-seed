/**
 * Dependencies
 */
import {takeEvery, put} from 'redux-saga/effects'

/**
 * Modules
 */
import Actions from './actions';

/**
 * Others
 */

/**
 * Code starts right below
 */

const SAGA_SAMPLE_ACTION = 'SAGA_ACTIONS_UPDATE';

/**
 * Saga
 * Export saga to be used by the middleware within the store.
 * Once declared this saga, immediately go to ./../../store/index.saga.js to import to the rootSaga
 */

export default [
  _watchDoSample()
];

function* _watchDoSample() {
  yield takeEvery(SAGA_SAMPLE_ACTION, _doSample);
}

function* _doSample(action) {
  yield put(Actions.doSample(action.data));
}

/**
 * API
 * Exported APIs used by containers to integrate with Saga
 */

export {
  doSample
};

function doSample(userData) {
  return {
    type: SAGA_SAMPLE_ACTION,
    data: userData
  };
}