/**
 * actions.js acts as an action creator which fires actions which will catched by a reducer to update the store
*/

/**
 * Dependencies
 */
import CONSTANTS from './constants';

/**
 * Modules
 */

/**
 * Others
 */

/**
 * Code starts right below
 */

export default {
  /**
   * This action creator is used within saga.js to fire actions to update the store via reducer.js
   */
  doSample: (userData) => {
    // The object return by an action creator must be a PURE JS object and contain the following properties as a convention with reducer.js
    // - type: a string representing an action which is defined in the constant.
    // - data: user data which will be used to update the store
    return {
      type: CONSTANTS.ACTIONS.SAMPLE_ACTION,
      data: userData
    };
  }
};