/**
 * Code starts right below
 */

const CONSTANT = {
  /**
   * This is the very initial data of a specific state
   */
  GENESIS_STATE: {},

  /**
   * This defines actions used by either an action creator or a reducer
   */
  ACTIONS: {
    SAMPLE_ACTION: 'SAMPLE_ACTION'
  }
};

export default CONSTANT;