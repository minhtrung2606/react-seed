/**
 * Dependencies
 */
import {connect} from 'react-redux';

/**
 * Components
 */
import SampleComp from './../../components/SampleComp';

/**
 * Modules
 */
import {doSample} from './saga';

/**
 * Others
 */

/**
 * Code starts right below
 */

/**
 * This is to map data from the store to the component wrapped inside this redux HOC component.
 * This must return a PURE JS object
 */
const mapStateToProps = state => {
  return {};
};

/**
 * This is to map functions to the component wrapped inside this redux HOC component.
 * This must return a PURE JS object containing wrapper functions. These wrapper functions works as a dispatcher to dispatch saga action.
 * `doSample` used inside dispatch method is implemented in saga.js which is in charge of dispatching logic.
 */
const mapDispatchToProps = dispatch => {
  return {
    doSample: userData => {
      dispatch(doSample(userData));
    }
  };
};

export default connect(mapStateToProps)(SampleComp);
