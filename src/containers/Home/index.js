/**
 * Dependecies
 */
import {connect} from 'react-redux';

/**
 * Components to be wrapped within Redux
 */
import Home from '../../components/Home';

/**
 * Modules
 */
import {updateUser} from './../User/saga';

/**
 * Code starts right below
 */

const mapStateToProps = state => {
  let {user} = state;
  return {user};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => {
      dispatch(updateUser(user));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);