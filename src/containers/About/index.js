/**
 * Dependencies
 */
import {connect} from 'react-redux';

/**
 * Components
 */
import About from '../../components/About';

/**
 * Code starts right below
 */

const mapStateToProps = state => {
  let {user} = state;
  return {user};
};

export default connect(mapStateToProps)(About);