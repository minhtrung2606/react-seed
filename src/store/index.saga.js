/**
 * Dependencies
 */
import {all} from 'redux-saga/effects'

/**
 * Modules
 */
import UserSagas from './../containers/User/saga';
import SampleCrudSagas from './../containers/SampleCrud/saga';

/**
 * Code starts right below
 */

export default function* rootSaga() {
  yield all([
    ...UserSagas,
    ...SampleCrudSagas
  ]);
};