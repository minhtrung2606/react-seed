/**
 * Dependecies
 */
import {createStore, combineReducers, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';

/**
 * Modules
 */
import user from './../containers/User/reducer';
import sampleCrud from './../containers/SampleCrud/reducer';
import rootSaga from './index.saga';

/**
 * Code starts right below
 */

const reducers = combineReducers({
  user,
  sampleCrud
});

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;