/**
 * Dependencies
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

/**
 * Components/Containers
 */
import RootApp from './components/Root';

/**
 * Modules
 */
import './index.css';
import store from './store/index';
import 'bootstrap/dist/css/bootstrap.min.css';

/**
 * Others
 */
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Provider store={store}>
    <RootApp/>
  </Provider>
, document.getElementById('root'));
registerServiceWorker();
